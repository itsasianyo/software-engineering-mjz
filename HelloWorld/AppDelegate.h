//
//  AppDelegate.h
//  HelloWorld
//
//  Created by Justin Rhea on 1/28/15.
//  Copyright (c) 2015 Justin Rhea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

